'use strict'
let nunjucks = require('nunjucks');

function createEnv(path,opts){
    path = path || 'views'
    opts = opts || {}

    let envOptionos={
        autoescape:opts.autoescape===undefined ? true : opts.autoescape
    }

    let env = nunjucks.configure(path,envOptionos)
    return env
}

module.exports=async function(ctx,next){
    ctx.render = function(view,model){
        let env = createEnv('views',{autoescape:false})
        ctx.body = env.render(view,model)
    }
    await next()
}