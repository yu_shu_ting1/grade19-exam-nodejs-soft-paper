'use strict'

let fn_1 = async(ctx,next)=>{
    ctx.body='🐟第一11111111111111111111111111111'
}

let fn_2 = async(ctx,next)=>{
    ctx.body='🐖第二22222222222222222222222222222'
}

let fn_3 = async(ctx,next)=>{
    ctx.body='🐂第三33333333333333333333333333333'
}

let fn_4 = async(ctx,next)=>{
    ctx.body='🐎第四44444444444444444444444444444'
}

module.exports={
    '/one':['get',fn_1],
    '/two':['get',fn_2],
    '/three':['get',fn_3],
    '/four':['get',fn_4]
}